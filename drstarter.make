api = 2
core = 7.22

; Basic contrib modules

projects[] = admin
projects[] = devel
projects[] = views
projects[] = ctools
projects[] = panels
projects[] = coder
projects[] = views_bulk_operations
projects[] = rules
projects[] = variable
projects[] = diff
projects[] = webform
projects[] = context
projects[] = mollom
projects[] = spambot
projects[] = wysiwyg
projects[] = wysiwyg_filter
projects[] = better_formats
projects[] = extlink
projects[] = omega_tools
projects[] = delta
projects[] = fontyourface
projects[] = block_class

libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][download][md5] = "2c37f261830e22fe2bfc3ee8fb576584"

; Basic contrib themes

projects[] = tao
projects[] = rubik
projects[] = omega

